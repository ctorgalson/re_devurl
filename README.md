# RE Dev URL

## What does this module do?

This tool is designed to help prevent development URLs from entering
content prior to a site's launch by informing users that they've made an
error.

This is prefereable to running find-and-replace queries on various
database tables just prior to launch (or at least should reduce the
number of such queries we *need* to run).

It utilizes `hook_node_validate()` to check fields (currently, this is
restricted to the 'body' field) for occurrences of one or more 'dev
urls' defined in the module's settings page.

If any instances of a dev url are found, the module can do one of two
things:

- display an error message using `form_set_error()` (this is the default
  and prevents the node from saving), or
- display an error message using `drupal_set_message()` (this behaviour
  allows the node to save, and can be enabled by *unchecking* the "Enable strict
  checking" box on the module's settings page)

The module's help page can also display a contact link for end users who
need more help determining when to use links containing a
top-level-domain and when not to.

### Note:

This module uses regular expressions to parse nodes when they are saved;
this is not especially performant. The module should therefore be
uninstalled and removed prior to site launch.

## Hooks

The module implements the following hooks:

- `hook_help()`
  Used to display help messages to end users. Can optionally display a
  `mailto` link to allow users to contact somebody for additional
  assistance.
- `hook_menu()`
  Used to display the module settings page at
  `admin/config/content/re_devurl`.
- `hook_node_validate()`
  Used to actually check for instances of the development url(s) in
  content on node save.
- `hook_permission()`
  Used to determine who can change this module's settings.
- `hook_theme()`
  Used to provide a theming function so that the error messages produced
  by the module are identical wherever they appear, or however they are
  displayed (they can appear as Drupal messages, or on the help page,
  and they may be output by `form_set_error()` or `drupal_set_message()`).

